import RPi.GPIO as GPIO
from time import sleep
import os
import time

GPIO.setmode(GPIO.BCM)

# LED 1
GPIO.setup(21, GPIO.OUT) # A
GPIO.setup(16, GPIO.OUT) # B
GPIO.setup(12, GPIO.OUT) # C
GPIO.setup(20, GPIO.OUT) # D
GPIO.setup(13, GPIO.OUT) # LD

# LED 2
GPIO.setup(24, GPIO.OUT) # A
GPIO.setup(18, GPIO.OUT) # B
GPIO.setup(25, GPIO.OUT) # C
GPIO.setup(23, GPIO.OUT) # D
GPIO.setup(6, GPIO.OUT) # LD

# LED 3
GPIO.setup(4, GPIO.OUT) # A
GPIO.setup(27, GPIO.OUT) # B
GPIO.setup(22, GPIO.OUT) # C
GPIO.setup(17, GPIO.OUT) # D
GPIO.setup(5, GPIO.OUT) # LD

# Setting LD on, which allows us to change the value of the led
GPIO.output(13, True)
GPIO.output(6, True)
GPIO.output(5, True)
# Setting LED 1 to 0
GPIO.output(21, False)
GPIO.output(16, False)
GPIO.output(12, False)
GPIO.output(20, False)
# Setting LED 2 to 0
GPIO.output(24, False)
GPIO.output(18, False)
GPIO.output(25, False)
GPIO.output(23, False)
# Setting LED 3 to 0
GPIO.output(4, False)
GPIO.output(27, False)
GPIO.output(22, False)
GPIO.output(17, False)

# IR Detector
GPIO.setup(26, GPIO.IN)

# Converts decimal number to binary number
def intToBinArray(value):
	returnValue = [0, 0, 0, 0]
	if (value > 9):
		return returnValue
	
	binaryStr = str(bin(value)[2:])
	# If the binaryStr is shorter than 3 characters, then adding zeros to the beginning 111 => 0111
	if (len(binaryStr) < len(returnValue)):
		zeroAmount = len(returnValue) - len(binaryStr)
		for i in range(zeroAmount):
			binaryStr = '0' + binaryStr
	
	for i in range(len(binaryStr)):
		returnValue[i] = int(binaryStr[i]) 
	
	return returnValue

# Set's a specific LED's number
def setLEDValue(LED, number):
	# Converting number to binary array
	binArray = intToBinArray(number)
	if (len(binArray) != 4): # Has to be 4-bit value
		print 'Error, number is not 4-bit'
		return 0
	if (LED == 1):
		GPIO.output(20, binArray[0])
		GPIO.output(12, binArray[1])
		GPIO.output(16, binArray[2])
		GPIO.output(21, binArray[3])
	elif (LED == 2):
		GPIO.output(23, binArray[0])
		GPIO.output(25, binArray[1])
		GPIO.output(18, binArray[2])
		GPIO.output(24, binArray[3])
	elif (LED == 3):
		GPIO.output(17, binArray[0])
		GPIO.output(22, binArray[1])
		GPIO.output(27, binArray[2])
		GPIO.output(4, binArray[3])

def closeLED(LED):
	# The led can be closed, by setting a binary value larger than 1001
	# Setting value 1111
	if (LED == 1):
		GPIO.output(20, 1)
		GPIO.output(12, 1)
		GPIO.output(16, 1)
		GPIO.output(21, 1)
	elif (LED == 2):
		GPIO.output(23, 1)
		GPIO.output(25, 1)
		GPIO.output(18, 1)
		GPIO.output(24, 1)
	elif (LED == 3):
		GPIO.output(17, 1)
		GPIO.output(22, 1)
		GPIO.output(27, 1)
		GPIO.output(4, 1)


def setNumber(number, visibleZeros = True):
	if (number > 999):
		print 'Error: Number can\'t be larger than 999'
		return 0
	numberStr = str(number)
	
	# If value has less than 3 digits, then zeros will be added 86 => 086
	if (len(numberStr) < 3):
		zeroAmount = 3 - len(numberStr)
		for i in range(zeroAmount):
			numberStr = '0' + numberStr
	
	# If visibleZeros is False, then the zeros before the value will not be shown
	if (visibleZeros == False and int(numberStr[0]) == 0):
		closeLED(3)
	else:
		setLEDValue(3, int(numberStr[0]))
	if (visibleZeros == False and int(numberStr[0]) == 0 and int(numberStr[1]) == 0):
		closeLED(2)
	else:
		setLEDValue(2, int(numberStr[1]))
	setLEDValue(1, int(numberStr[2]))
	

def RCtime (RCpin):
	reading = 0
	GPIO.setup(RCpin, GPIO.OUT)
	GPIO.output(RCpin, GPIO.LOW)
	time.sleep(0.1)
	
	GPIO.setup(RCpin, GPIO.IN)
	# This takes about 1 millisecond per loop cycle
	while (GPIO.input(RCpin) == False):
		reading += 1
	return reading

try:
	'''
	# Getting the time it takes for the capacitor to charge. 
	# The longer it takes, the smaller the IR light is
	IRDetected = True
	IRTimeLimit = 1700
	currentNumber = 0
	
	# Waiting until we see the IR (RCtime < 500)
	while (RCtime(26) >= IRTimeLimit):
		sleep(0.1)
	print 'Detected'
	print IRDetected
	
	while True:
		capacitorTime = RCtime(26)
		print capacitorTime
		print IRDetected
		if (IRDetected == True and capacitorTime > IRTimeLimit): # If we earlier saw the IR, but not anymore, that means someone is in the way
			IRDetected = False
			print 'lost'
		elif (IRDetected == False and capacitorTime < IRTimeLimit): # If we earlier couldn't see the IR (someone is in the way) but can not again, then that person has moved away and we need to count one more person
			IRDetected = True
			print 'found'
			currentNumber = currentNumber + 1
			setNumber(currentNumber)
	
	'''
	
	sleepTime = 1.0
	# From 0 - 999
	while True:
		for i in range(0, 1000):
			setNumber(i)
			sleep(sleepTime)
			sleepTime = sleepTime*0.99
	
	'''
	print 'starting'
	
	while True:
		sleep(1)
		if (GPIO.input(26) == True):
			print 'Detected'
		else:
			print 'Not Detected'
	'''
finally:
	GPIO.cleanup()
